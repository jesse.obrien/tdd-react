import React, { Component } from 'react';
import { shallow, mount } from 'enzyme';
import { expect, assert } from 'chai';

import CommentForm from '../CommentForm.jsx';

describe('<CommentForm />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<CommentForm />);
    });

    it('Should have text input for author', () => {
        expect(wrapper.find('input[type="text"]').props().name).to.equal('author');
    });

    it('Should have text input for comment text', () => {
        expect(wrapper.find('textarea').props().name).to.equal('comment');
    });

    it('Submit button should call API to post comment', () => {
        wrapper = mount(<CommentForm />);

        expect(wrapper.find('button').text()).to.equal("Submit");

        expect(wrapper.state('apiCalled')).to.equal(false);

        wrapper.find('button').simulate('click');

    });

    it('Should update state with new comment when button pressed', () => {
        let author = "Ernest Cline";

        expect(wrapper.state('author')).to.equal('');

        wrapper.find('input[type="text"]').simulate('change', {target: {value: author}});

        expect(wrapper.state('author')).to.equal(author);
    });

    it('Should update state when textarea value changes', () => {
        let comment = "You should read Ready Player One!";

        expect(wrapper.state('comment')).to.equal('');

        wrapper.find('textarea').simulate('change', {target: {value: comment}});

        expect(wrapper.state('comment')).to.equal(comment);
    });
});

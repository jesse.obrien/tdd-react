import React, { Component } from 'react';
import { shallow } from 'enzyme';
import { expect, assert } from 'chai';

import Comment from '../Comment.jsx';

describe('<Comment />', () => {
    let wrapper;

    it('Should render an author', () => {
        let author = "Chris Keithlin";

        wrapper = shallow(<Comment author={author} />);
        expect(wrapper.find('h3').contains(author), 'I has an author').to.equal(true);
    });

    it('Should render a comment from the author', () => {
        let comment = "I love vehikl!";

        wrapper = shallow(<Comment comment={comment} />);
        assert(wrapper.find('p').contains(comment));
    });

});

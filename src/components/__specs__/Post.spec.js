import React, { Component } from 'react';
import { shallow } from 'enzyme';
import { expect, assert } from 'chai';

import Post from '../Post.jsx';
import Comment from '../Comment.jsx';

describe('<Post />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Post />);
    });

    it('Should render a post component', () => {
        expect(wrapper.type()).to.equal('div');
        assert(wrapper.hasClass('post'));
    });

    it('Should render an authors name', () => {
        let authorName = "Elon Musk";

        wrapper = shallow(<Post author={authorName} />);

        let author = wrapper.find('h1');
        assert(author.contains(authorName));
        assert(author.hasClass('author'));
    });

    it('Should render post text', () => {
        let text = "I love SpaceX!";

        wrapper = shallow(<Post author="Elon Musk" text={text} />);

        let post = wrapper.find('p');
        assert(post.contains(text));
        expect(post.hasClass('body')).to.equal(true);
    });

    it('Should render multiple comments', () => {
        let comments = [
            { id: 1, author: "Elon Musk", comment: "I love spacex" },
            { id: 2, author: "Jeff Bezos", comment: "Spacex Sucks" }
        ];

        wrapper = shallow(<Post author="Elon Musk"
            text="This is a post"
            comments={comments} />);

        expect(wrapper.find(Comment)).to.have.length(2);
    });

});

import React, { Component } from 'react';

import Comment from './Comment.jsx';

export default class Post extends Component {
    render() {
        return (
            <div className="post">
                <h1 className="author">{this.props.author}</h1>
                <p className="body">{this.props.text}</p>
                <div>
                    {this.props.comments && this.props.comments.map(comment => {
                        return <Comment key={comment.id} 
                            author={comment.author} 
                            comment={comment.comment} />;
                    })}
                </div>
            </div>
        );
    }
}

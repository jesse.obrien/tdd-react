import React, { Component } from 'react';

export default class CommentForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            author: '',
            comment: '',
            apiCalled: false
        };
    }
    handleSubmit(value) {
        console.log('waaaaaaaaaaaaaaaaaaaaaaat');
        this.setState({
            apiCalled: true
        });
    }
    render() {
        return (
            <div>
                <form ref="myForm">
                    <input type="text"
                        name="author"
                        onChange={(changeEvent) => {
                            this.setState({
                                author: changeEvent.target.value
                            });
                        }}
                    />

                    <textarea id="comment"
                        name="comment"
                        cols="30"
                        rows="10"
                        onChange={(changeEvent) => {
                            this.setState({
                                comment: changeEvent.target.value
                            });
                        }}
                        value={this.state.comment}
                    />

                    <button id="submitComment"
                        onClick={() => this.handleSubmit}
                    >Submit</button>
                </form>
            </div>
        );
    }
}

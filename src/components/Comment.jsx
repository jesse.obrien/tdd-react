import React, { Component } from 'react';

export default class Comment extends Component {
    render() {
        return (
            <div>
                <h3>{this.props.author}</h3>
                <p>{this.props.comment}</p>
            </div>
        );
    }
}
